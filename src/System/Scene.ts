import { Game, Behavior, Layer, Entity, IEntityOptions } from "./";
import { clamp } from "../Math";

export class Scene {
	private behaviors: Map<typeof Behavior, Behavior[]> = new Map();
	private layers: Layer[] = [];

	constructor(readonly game: Game) {
		this.layers.push(new Layer("Background"));
	}

	public tickBehaviors() {
		for(let v of this.behaviors.values()) {
			for(let b of v) {
				b.Tick(this.game.dt);
			};
		};
	}

	public draw() {
		const ctx = this.game.renderer.ctx;
		for(let l of this.layers) {
			for(let e of l.entities) {
				e.draw(ctx);
			}
		}
	}

	public addBehaviorToScene(ctor: new(opts: any, owner: any) => Behavior, beh: Behavior): void {
		const behArr = this.behaviors.get(ctor);
		if(!behArr) {
			this.behaviors.set(ctor, [beh]);
		}
		else {
			behArr.push(beh);
		}
		beh.OnCreate();
	}

	public createEntity<T extends Entity>(ctor: new(opts: IEntityOptions, game: Game) => T, options: IEntityOptions = {}): T {
		const e = new ctor(options, this.game);

		let layerIndex = this.layers.length - 1;
		if(typeof options.layer === "string") {
			for(let i = 0; i < this.layers.length; i++) {
				if(this.layers[i].name === options.layer) {
					layerIndex = i;
					break;
				}
			}
		}
		if(typeof options.layer === "number") {
			layerIndex = clamp(options.layer, 0, this.layers.length - 1);
		}

		this.layers[layerIndex].addEntity(e);

		return e;
	}

	addLayer(name: string, index?: number) {
		index = (typeof index === "number") ? index : this.layers.length;
		const l = new Layer(name);
		this.layers.splice(index, 0, l);
	}
}
