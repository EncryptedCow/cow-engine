import { CanvasRenderer, IRendererOptions } from "../Graphics";
import { Scene } from "./";

export class Game {
	readonly renderer: CanvasRenderer;
	readonly scene: Scene;

	private lastUpdate: number;
	private _dt: number = 0;
	public get dt() { return this._dt; }
	private _fps: number = 0;
	public get fps() { return this._fps; }

	constructor(opts: IGameOptions) {
		this.renderer = new CanvasRenderer(opts, this);
		this.scene = new Scene(this);

		this.gameLoop = this.gameLoop.bind(this);
		this.lastUpdate = performance.now();
		requestAnimationFrame(this.gameLoop);
	}

	private gameLoop(now: number) {
		const dt = (now - this.lastUpdate) / 1000
		this.lastUpdate = now;
		this._dt = dt;
		this._fps = Math.max(0, (Math.round(1 / dt)));

		this.scene.tickBehaviors();
		this.renderer.clear();
		this.scene.draw();

		requestAnimationFrame(this.gameLoop);
	}
}

export interface IGameOptions extends IRendererOptions {}
