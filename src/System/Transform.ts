import { Vec2 } from "../Math";

export class Transform {
	public position: Vec2;
	private _angle: number = 0;
	public get angle() { return this._angle; }
	public set angle(a: number) {
		const modA = a % 360;
		this._angle = modA < 0 ? 360 - modA : modA;
	}
	public scale: Vec2;
	public origin: Vec2;
	private _layer: string = "";
	public get layer() { return this._layer; }

	constructor(opts: ITransformOptions = {}) {
		this.position = opts.position || Vec2.Zero;
		this.angle = (typeof opts.angle === "number") ? opts.angle : 0;
		this.scale = opts.scale || Vec2.One;
		this.origin = opts.origin || Vec2.Zero;
	}
}

export interface ITransformOptions {
	position?: Vec2;
	angle?: number;
	origin?: Vec2;
	scale?: Vec2;
}
