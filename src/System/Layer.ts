import { Entity } from "./";

export class Layer {
	private _entites: Entity[] = [];
	public get entities(): ReadonlyArray<Entity> {
		return this._entites;
	}
	
	constructor(readonly name: string) {}

	addEntity(entity: Entity, nextTo?: Entity, behind = true) {
		let i: number;
		if(nextTo && (i = this._entites.indexOf(nextTo)) > -1) { // pls dont hate me for this
			this._entites.splice(i + (behind ? 0 : 1), 0, entity); // if behind is false, add 1 to splice index
			return;
		}
		this._entites.push(entity);
	}
}
