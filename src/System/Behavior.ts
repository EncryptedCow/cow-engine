import { Entity } from "./";

export abstract class Behavior {

	constructor(opts: IBehaviorOptions, readonly owner: Entity) {}

	public OnCreate() {}
	public Tick(dt: number) {}
	// public Tick2() {}
}

export interface IBehaviorOptions {
	name: string
}
