import { Game, Transform, ITransformOptions, Behavior, IBehaviorOptions } from "./";

export abstract class Entity {
	readonly transform: Transform;
	private behaviors: Map<string, Behavior> = new Map();

	constructor(opts: IEntityOptions, readonly game: Game) {
		this.transform = new Transform(opts);
	}

	public abstract draw(ctx: CanvasRenderingContext2D): void;

	addBehavior<T extends Behavior>(ctor: new(opts: IBehaviorOptions, owner: Entity) => T, opts: IBehaviorOptions): T | null {
		if(this.behaviors.has(opts.name)) {
			console.error(`A behavior with the name '${opts.name}' already exists.`);
			return null
		}

		const b = new ctor(opts, this);
		this.behaviors.set(opts.name, b);

		this.game.scene.addBehaviorToScene(ctor, b);
		return b;
	}

	getBehavior<T extends Behavior>(name: string): T | null {
		return <T>this.behaviors.get(name) || null;
	}
}

export interface IEntityOptions extends ITransformOptions {
	layer?: number | string
}
