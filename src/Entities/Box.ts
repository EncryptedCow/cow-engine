import { Entity } from "../System";
import { Colour } from "../Graphics";
import { transformToEntity } from "../Math";

export class Box extends Entity {
	public colour: Colour = Colour.Red;
	public width: number = 32;
	public height: number = 32;

	public draw(ctx: CanvasRenderingContext2D) {
		ctx.save();

		ctx.fillStyle = this.colour.toRGBA();
		transformToEntity(ctx, this);
		ctx.fillRect(0, 0, this.width, this.height);

		ctx.restore();
	}
}

Cow.registerEntity("Box", Box);
