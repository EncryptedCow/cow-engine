import { Entity, Behavior } from "./System";

abstract class _Cow {
	private static _entities: Map<string, typeof Entity> = new Map();
	private static _behaviors: Map<string, typeof Behavior> = new Map();
	
	public static registerEntity(name: string, ctor: typeof Entity) {
		this._entities.set(name, ctor);
	}

	public static getEntityClass<T extends typeof Entity>(name: string): T | null {
		return <T>this._entities.get(name) || null;
	}
	
	public static registerBehavior(name: string, ctor: typeof Behavior) {
		this._behaviors.set(name, ctor);
	}

	public static getBehaviorClass<T extends typeof Behavior>(name: string): T | null {
		return <T>this._behaviors.get(name) || null;
	}
}

export default _Cow;
(<any>window).Cow = _Cow;

declare global {
	const Cow: typeof _Cow
}
