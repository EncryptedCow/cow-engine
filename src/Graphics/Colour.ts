export class Colour {
	static get Black() { return new Colour(0, 0, 0, 1); }
	static get White() { return new Colour(255, 255, 255, 1); }
	static get Red()   { return new Colour(255, 0, 0, 1); }
	static get Green() { return new Colour(0, 255, 0, 1); }
	static get Blue()  { return new Colour(0, 0, 255, 1); }
	static get CornflowerBlue()  { return new Colour(100, 149, 237, 1); }
	
	constructor(public r: number, public g: number, public b: number, public a: number) {}

	public set(r: number, g: number, b: number, a?: number) {
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a || this.a;
	}

	public equals(c: Colour): boolean {
		return this.r === c.r && this.g === c.g && this.b === c.b && this.a === c.a;
	}

	public equalsRGB(c: Colour): boolean {
		return this.r === c.r && this.g === c.g && this.b === c.b;
	}

	public toRGB(): string {
		return `rgb(${this.r},${this.g},${this.b})`;
	}

	public toRGBA(): string {
		return `rgba(${this.r},${this.g},${this.b},${this.a})`;
	}
}

export const Color = Colour;
