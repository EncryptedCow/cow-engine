import { Colour } from "./Colour";
import { Game } from "../System";

export class CanvasRenderer {
	private _can: HTMLCanvasElement;
	public get canvas() { return this._can; }

	private _ctx: CanvasRenderingContext2D;
	public get ctx() { return this._ctx; }

	private _clearColour: Colour;
	public get clearColour() { return this._clearColour; }
	public set clearColour(c: Colour) {
		if(!this._clearColour.equals(c)) {
			this._clearColour = c;
		}
	}

	constructor(opts: IRendererOptions, readonly game: Game) {
		const ctx = opts.canvas.getContext("2d");
		if(!ctx) {
			throw new Error("Failed to get canvas context??? Are you using a potato?");
		}
		opts.canvas.width = opts.width;
		opts.canvas.height = opts.height;
		this._can = opts.canvas;
		this._ctx = ctx;

		this._clearColour = opts.clearColour || Colour.CornflowerBlue;
		this.clear();
	}

	public clear(): void {
		this._ctx.fillStyle = this._clearColour.toRGBA();
		this._ctx.fillRect(0, 0, this._can.width, this._can.height);
	}
}

export interface IRendererOptions {
	canvas: HTMLCanvasElement;
	width: number;
	height: number;
	clearColour?: Colour;
}
