import { Colour } from "./Colour";

export class WebGLRenderer {
	private _gl: WebGL2RenderingContext;
	public get gl(): WebGL2RenderingContext { return this._gl; }

	private _can: HTMLCanvasElement;
	public get canvas() { return this._can; }

	private _clearColour: Colour;
	public get clearColour() { return this._clearColour; }
	public set clearColour(c: Colour) {
		if(!this._clearColour.equals(c)) {
			this._clearColour = c;
			this.updateClearColour();
		}
	}

	constructor(opts: IRendererOptions) {
		const attrbs: WebGLContextAttributes = {
			antialias: (typeof opts.antialias === "boolean") ? opts.antialias : true
		}
		
		const gl = opts.canvas.getContext("webgl2", attrbs);
		if(!gl) {
			throw new Error(`Failed to get WebGL2 context :(`);
		}
		this._gl = gl;
		this._can = opts.canvas;

		this._clearColour = opts.clearColour || Colour.CornflowerBlue;
		this.updateClearColour();
		this.clear();
	}

	private updateClearColour(): void {
		const cc = this._clearColour;
		this._gl.clearColor(cc.r/255, cc.g/255, cc.b/255, cc.a);
	}

	public clear(): void {
		this._gl.clear(this._gl.COLOR_BUFFER_BIT | this._gl.DEPTH_BUFFER_BIT);
	}
}

export interface IRendererOptions {
	canvas: HTMLCanvasElement;
	antialias?: boolean;
	clearColour?: Colour;
}
