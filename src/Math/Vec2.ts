export class Vec2 {
	static get Zero()  { return new Vec2(0, 0); }
	static get One()   { return new Vec2(1, 1); }
	static get Up()    { return new Vec2(0,-1); }
	static get Down()  { return new Vec2(0, 1); }
	static get Left()  { return new Vec2(-1,0); }
	static get Right() { return new Vec2(1, 0); }

	constructor(public x: number, public y: number) {}

	static random(scale = 1.0): Vec2 {
		const r = Math.random() * 2.0 * Math.PI;
		return new Vec2(Math.cos(r) * scale, Math.sin(r) * scale);
	}

	public copy(v: Vec2): Vec2 {
		this.x = v.x;
		this.y = v.y;
		return this;
	}

	public clone(): Vec2 {
		return new Vec2(this.x, this.y);
	}

	public set(x: number, y: number): Vec2 {
		this.x = x;
		this.y = y;
		return this;
	}

	//#region "Math"

	public add(v: Vec2): Vec2 {
		this.x += v.x;
		this.y += v.y;
		return this;
	}

	public addS(x: number, y: number): Vec2 {
		this.x += x;
		this.y += y;
		return this;
	}

	public sub(v: Vec2): Vec2 {
		this.x -= v.x;
		this.y -= v.y;
		return this;
	}

	public subS(x: number, y: number): Vec2 {
		this.x -= x;
		this.y -= y;
		return this;
	}

	public mul(v: Vec2): Vec2 {
		this.x *= v.x;
		this.y *= v.y;
		return this;
	}

	public mulS(x: number, y: number): Vec2 {
		this.x *= x;
		this.y *= y;
		return this;
	}

	public div(v: Vec2): Vec2 {
		this.x /= v.x;
		this.y /= v.y;
		return this;
	}

	public divS(x: number, y: number): Vec2 {
		this.x /= x;
		this.y /= y;
		return this;
	}

	public round(): Vec2 {
		this.x = Math.round(this.x);
		this.y = Math.round(this.y);
		return this;
	}

	public floor(): Vec2 {
		this.x = Math.floor(this.x);
		this.y = Math.floor(this.y);
		return this;
	}

	public ceil(): Vec2 {
		this.x = Math.ceil(this.x);
		this.y = Math.ceil(this.y);
		return this;
	}

	//#endregion "Math"

	public get magnitude(): number {
		return Math.sqrt(this.x * this.x + this.y * this.y);
	}

	public get magnitudeSquared(): number {
		return this.x * this.x + this.y * this.y;
	}

	public normalize(): Vec2 {
		let len = this.magnitudeSquared;
		if(len > 0) {
			len = 1 / Math.sqrt(len);
		}
		this.x *= len;
		this.y *= len;
		return this;
	}

	public get normalized(): Vec2 {
		return this.clone().normalize();
	}

	public distance(v: Vec2): number {
		const x = v.x - this.x;
		const y = v.y - this.y;
		return Math.sqrt(x * x + y * y);
	}

	public distanceSquared(v: Vec2): number {
		const x = v.x - this.x;
		const y = v.y - this.y;
		return x * x + y * y;
	}

	public equals(v: Vec2): boolean {
		return (Math.abs(this.x - v.x) <= Number.EPSILON * Math.max(1.0, Math.abs(this.x), Math.abs(v.x)) &&
				Math.abs(this.y - v.y) <= Number.EPSILON * Math.max(1.0, Math.abs(this.y), Math.abs(v.y)));
	}

	public exactEquals(v: Vec2): boolean {
		return this.x === v.x && this.y === v.y;
	}

	public negate(): Vec2 {
		this.x = -this.x;
		this.y = -this.y;
		return this;
	}

	public toString(): string {
		return `Vec2(${this.x}, ${this.y})`;
	}
}
