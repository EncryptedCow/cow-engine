import { Entity } from "../System";

export * from "./Vec2";

export function toRad(d: number): number {
	return d * (Math.PI / 180);
}

export function toDeg(r: number): number {
	return r * (180 / Math.PI);
}

export function transformToEntity(ctx: CanvasRenderingContext2D, entity: Entity) {
	const t = entity.transform;
	ctx.translate(t.position.x, t.position.y);
	ctx.rotate(toRad(t.angle));
	ctx.translate(-t.origin.x, -t.origin.y);
	ctx.scale(t.scale.x, t.scale.y);
}

export function clamp(n: number, min: number, max: number): number {
	return Math.max(min, Math.max(max, n));
}
