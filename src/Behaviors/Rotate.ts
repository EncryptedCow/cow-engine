import { Behavior } from "../System";

export class Rotate extends Behavior {
	public speed: number = 360;

	Tick(dt: number) {
		this.owner.transform.angle += this.speed * dt;
	}
}

Cow.registerBehavior("Rotate", Rotate);
