const gulp = require("gulp");
const sourcemaps = require("gulp-sourcemaps");
const ts = require("gulp-typescript");
const del = require("del");
const header = require("gulp-header");
const footer = require("gulp-footer");

gulp.task("compile", () => {
	const tsProject = ts.createProject("tsconfig.json");

	return tsProject.src()
		.pipe(sourcemaps.init())
		.pipe(tsProject())
		.pipe(sourcemaps.write())
		.pipe(gulp.dest("./build"));
});

gulp.task("copy:glsl", () => {
	return gulp.src("./src/**/*.glsl")
		.pipe(header("exports.default = \""))
		.pipe(footer("\";"))
		.pipe(gulp.dest("./build"));
});

gulp.task("watch", () => {
	gulp.watch("./src/**/*.glsl", gulp.series("copy:glsl"));
	gulp.watch("./src/**/*.ts", gulp.series("compile"));
});

gulp.task("clean", () => {
	return del("./build");
});

gulp.task("build", gulp.series("clean", "copy:glsl", "compile"));
gulp.task("dev", gulp.series("copy:glsl", "compile", "watch"));